%global debug_package %{nil}
%global prjname zenpower3
%global commit 138fa0637b46a0b0a087f2ba4e9146d2f9ba2475
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Name: %{prjname}
Version: 0.2.0^3.git%{shortcommit}
Release: 1%{?dist}
Summary: Linux driver for reading temperature, voltage(SVI2), current(SVI2) and power(SVI2) for AMD Zen CPUs including Zen 3

License: GPLv2
URL: https://gitlab.com/shdwchn10/%{prjname}
Source0: modprobe.conf
Source1: modules-load.conf

BuildArch: noarch

BuildRequires: gcc
BuildRequires: make
BuildRequires: systemd-rpm-macros

Requires: %{name}-kmod >= %{version}

Provides: %{name}-kmod-common = %{version}-%{release}

%description
Zenpower3 is a Linux kernel driver for reading temperature, voltage(SVI2), current(SVI2) and power(SVI2) for AMD Zen family CPUs, now with Zen 3 support!


%install
install -Dpm 0644 %{S:0} %{buildroot}%{_modprobedir}/k10temp-blacklist.conf
install -Dpm 0644 %{S:1} %{buildroot}%{_modulesloaddir}/%{name}.conf


%files
%{_modprobedir}/k10temp-blacklist.conf
%{_modulesloaddir}/%{name}.conf


%changelog
* Fri Feb 28 2025 Andrey Brusnik <dev@shdwchn.io> - 0.2.0^3.git138fa06-1
- fix: Building on 6.14 kernel

* Thu Dec 19 2024 Andrey Brusnik <dev@shdwchn.io> - 0.2.0^2.gitc176fdb-3
- fix: Update broken link to mirror

* Sat Sep 28 2024 Andrey Brusnik <dev@shdwchn.io> - 0.2.0^1.gitc176fdb0d5-2
- fix: Set BuildArch to noarch

* Tue Nov 07 2023 Andrey Brusnik <dev@shdwchn.io> - 0.2.0^1.gitc176fdb0d5-1
- Initial package
